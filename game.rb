require 'colorize'

# Returns a key when just pressed (without Enter key)
def get_keypressed
	system("stty raw -echo")
	t = STDIN.getc
	system("stty -raw echo")
	return t
end

# Returns next token turn to play
def next_turn( turn )
    return (turn + 1) % 2
end

  # Show winner token
def token_wins( token )
    puts "            ¡GANADOR!"
    puts ""
    puts "             #{token}" 
    puts ""
    puts ""
end

def check_match(f1,f2,f3,f4,r1,r2,r3,r4, move, move2)

    value1 = 0
    value2 = 0


    if ((move[0]==0)&&(move[1]==0))
        value1 = r1[0].to_s
        
    elsif ((move[0]==0)&&(move[1]==1))
        value1 = r1[1].to_s    
        
    elsif ((move[0]==0)&&(move[1]==2))
        value1 = r1[2].to_s
        
    elsif ((move[0]==0)&&(move[1]==3))
        value1 = r1[3].to_s
        
    elsif ((move[0]==0)&&(move[1]==4))
        value1 = r1[4].to_s
        
    elsif ((move[0]==1)&&(move[1]==0))
        value1 = r2[0].to_s
        
    elsif ((move[0]==1)&&(move[1]==1))
        value1 = r2[1].to_s
       
    elsif ((move[0]==1)&&(move[1]==2))
        value1 = r2[2].to_s

    elsif ((move[0]==1)&&(move[1]==3))
        value1 = r2[3].to_s

    elsif ((move[0]==1)&&(move[1]==4))
        value1 = r2[4].to_s

    elsif ((move[0]==2)&&(move[1]==0))
        value1 = r3[0].to_s

    elsif ((move[0]==2)&&(move[1]==1))
        value1 = r3[1].to_s

    elsif ((move[0]==2)&&(move[1]==2))
        value1 = r3[2].to_s

    elsif ((move[0]==2)&&(move[1]==3))
        value1 = r3[3].to_s

    elsif ((move[0]==2)&&(move[1]==4))
        value1 = r3[4].to_s

    elsif ((move[0]==3)&&(move[1]==0))
        value1 = r4[0].to_s

    elsif ((move[0]==3)&&(move[1]==1))
        value1 = r4[1].to_s

    elsif ((move[0]==3)&&(move[1]==2))
        value1 = r4[2].to_s

    elsif ((move[0]==3)&&(move[1]==3))
        value1 = r4[3].to_s

    elsif ((move[0]==3)&&(move[1]==4))
        value1 = r4[4].to_s
    else
        value1 = value1
    end
    
    if ((move2[0]==0)&&(move2[1]==0))
        value2 = r1[0].to_s
        
    elsif ((move2[0]==0)&&(move2[1]==1))
        value2 = r1[1].to_s    
        
    elsif ((move2[0]==0)&&(move2[1]==2))
        value2 = r1[2].to_s
        
    elsif ((move2[0]==0)&&(move2[1]==3))
        value2 = r1[3].to_s
        
    elsif ((move2[0]==0)&&(move2[1]==4))
        value2 = r1[4].to_s
        
    elsif ((move2[0]==1)&&(move2[1]==0))
        value2 = r2[0].to_s
        
    elsif ((move2[0]==1)&&(move2[1]==1))
        value2 = r2[1].to_s
       
    elsif ((move2[0]==1)&&(move2[1]==2))
        value2 = r2[2].to_s

    elsif ((move2[0]==1)&&(move2[1]==3))
        value2 = r2[3].to_s

    elsif ((move2[0]==1)&&(move2[1]==4))
        value2 = r2[4].to_s

    elsif ((move2[0]==2)&&(move2[1]==0))
        value2 = r3[0].to_s

    elsif ((move2[0]==2)&&(move2[1]==1))
        value2 = r3[1].to_s

    elsif ((move2[0]==2)&&(move2[1]==2))
        value2 = r3[2].to_s

    elsif ((move2[0]==2)&&(move2[1]==3))
        value2 = r3[3].to_s

    elsif ((move2[0]==2)&&(move2[1]==4))
        value2 = r3[4].to_s

    elsif ((move2[0]==3)&&(move2[1]==0))
        value2 = r4[0].to_s

    elsif ((move2[0]==3)&&(move2[1]==1))
        value2 = r4[1].to_s

    elsif ((move2[0]==3)&&(move2[1]==2))
        value2 = r4[2].to_s

    elsif ((move2[0]==3)&&(move2[1]==3))
        value2 = r4[3].to_s

    elsif ((move2[0]==3)&&(move2[1]==4))
        value2 = r4[4].to_s
    else
        value2 = value1
    end

    if (value1==value2)
        return true
    else
        return false
    end
end

# Develar la carta en la pantalla

def show_card(f1, f2, f3, f4, r1, r2, r3, r4, move)    
              
    
        if ((move[0]==0)&&(move[1]==0))
            f1[0] = r1[0].to_s
            
        elsif ((move[0]==0)&&(move[1]==1))
            f1[1] = r1[1].to_s    
            
        elsif ((move[0]==0)&&(move[1]==2))
            f1[2] = r1[2].to_s
            
        elsif ((move[0]==0)&&(move[1]==3))
            f1[3] = r1[3].to_s
            
        elsif ((move[0]==0)&&(move[1]==4))
            f1[4] = r1[4].to_s
            
        elsif ((move[0]==1)&&(move[1]==0))
            f2[0] = r2[0].to_s
            
        elsif ((move[0]==1)&&(move[1]==1))
            f2[1] = r2[1].to_s
           
        elsif ((move[0]==1)&&(move[1]==2))
            f2[2] = r2[2].to_s

        elsif ((move[0]==1)&&(move[1]==3))
            f2[3] = r2[3].to_s

        elsif ((move[0]==1)&&(move[1]==4))
            f2[4] = r2[4].to_s

        elsif ((move[0]==2)&&(move[1]==0))
            f3[0] = r3[0].to_s

        elsif ((move[0]==2)&&(move[1]==1))
            f3[1] = r3[1].to_s

        elsif ((move[0]==2)&&(move[1]==2))
            f3[2] = r3[2].to_s

        elsif ((move[0]==2)&&(move[1]==3))
            f3[3] = r3[3].to_s

        elsif ((move[0]==2)&&(move[1]==4))
            f3[4] = r3[4].to_s

        elsif ((move[0]==3)&&(move[1]==0))
            f4[0] = r4[0].to_s

        elsif ((move[0]==3)&&(move[1]==1))
            f4[1] = r4[1].to_s

        elsif ((move[0]==3)&&(move[1]==2))
            f4[2] = r4[2].to_s

        elsif ((move[0]==3)&&(move[1]==3))
            f4[3] = r4[3].to_s

        elsif ((move[0]==3)&&(move[1]==4))
            f4[4] = r4[4].to_s

        else
          return false
    end
end

def cover_card(f1, f2, f3, f4, r1, r2, r3, r4, move)   
                
    
        if ((move[0]==0)&&(move[1]==0))
            f1[0] = "XX"
            
        elsif ((move[0]==0)&&(move[1]==1))
            f1[1] = "XX"    
            
        elsif ((move[0]==0)&&(move[1]==2))
            f1[2] = "XX"
            
        elsif ((move[0]==0)&&(move[1]==3))
            f1[3] = "XX"
            
        elsif ((move[0]==0)&&(move[1]==4))
            f1[4] = "XX"
            
        elsif ((move[0]==1)&&(move[1]==0))
            f2[0] = "XX"
            
        elsif ((move[0]==1)&&(move[1]==1))
            f2[1] = "XX"
           
        elsif ((move[0]==1)&&(move[1]==2))
            f2[2] = "XX"

        elsif ((move[0]==1)&&(move[1]==3))
            f2[3] = "XX"

        elsif ((move[0]==1)&&(move[1]==4))
            f2[4] = "XX"

        elsif ((move[0]==2)&&(move[1]==0))
            f3[0] = "XX"

        elsif ((move[0]==2)&&(move[1]==1))
            f3[1] = "XX"

        elsif ((move[0]==2)&&(move[1]==2))
            f3[2] = "XX"

        elsif ((move[0]==2)&&(move[1]==3))
            f3[3] = "XX"

        elsif ((move[0]==2)&&(move[1]==4))
            f3[4] = "XX"

        elsif ((move[0]==3)&&(move[1]==0))
            f4[0] = "XX"

        elsif ((move[0]==3)&&(move[1]==1))
            f4[1] = "XX"

        elsif ((move[0]==3)&&(move[1]==2))
            f4[2] = "XX"

        elsif ((move[0]==3)&&(move[1]==3))
            f4[3] = "XX"

        elsif ((move[0]==3)&&(move[1]==4))
            f4[4] = "XX"

        else
          return false
    end
end

def show_mainboard(f1, f2, f3, f4)       
    
    dash_sep = "                 +----+----+----+----+----+"
    pipe_sep = "|"
    space_tab = "               "
    cols = "                    1    2    3    4    5" 
    row1 = "1 "
    row2 = "2 "
    row3 = "3 "
    row4 = "4 "

    print "#{cols.to_s.colorize(:red)}"
    puts ""
    puts dash_sep.colorize(:yellow)    
    print "#{space_tab.colorize(:yellow)}"
    print "#{row1.colorize(:blue)}"
    for i in 0..f1.length - 1 do
        if (f1[i]=="XX")
            print pipe_sep.colorize(:yellow) + " #{f1[i]} "
        else
            print pipe_sep.colorize(:yellow) + " #{f1[i].to_s.colorize(:green)} "
        end
    end        
    puts pipe_sep.colorize(:yellow)
    puts dash_sep.colorize(:yellow)
    print "#{space_tab.colorize(:yellow)}"    
    print "#{row2.colorize(:blue)}"
    for i in 0..f2.length - 1 do
        if (f2[i]=="XX")
            print pipe_sep.colorize(:yellow) + " #{f2[i]} "        
        else
            print pipe_sep.colorize(:yellow) + " #{f2[i].to_s.colorize(:green)} "
        end
    end        
    puts pipe_sep.colorize(:yellow)
    puts dash_sep.colorize(:yellow)    
    print "#{space_tab.colorize(:yellow)}"
    print "#{row3.colorize(:blue)}"
    for i in 0..f3.length - 1 do
        if (f3[i]=="XX")
            print pipe_sep.colorize(:yellow) + " #{f3[i]} "
        else
            print pipe_sep.colorize(:yellow) + " #{f3[i].to_s.colorize(:green)} "
        end
    end        
    puts pipe_sep.colorize(:yellow)
    puts dash_sep.colorize(:yellow)   
    print "#{space_tab.colorize(:yellow)}"
    print "#{row4.colorize(:blue)}"
    for i in 0..f4.length - 1 do
        if (f4[i]=="XX")
            print pipe_sep.colorize(:yellow) + " #{f4[i]} "
        else
            print pipe_sep.colorize(:yellow) + " #{f4[i].to_s.colorize(:green)} "
        end
    end        
    puts pipe_sep.colorize(:yellow)
    puts dash_sep.colorize(:yellow)   
end

def update_screen(f1, f2, f3, f4, p1, p2)
    system("clear")
    puts "     +----------------------------------------------+"
    puts "     +                MEMORY  MATCH                 +"
    puts "     +----------------------------------------------+"
    print "     + Score Player 1: " "#{p1[0]} " "---------------------------+"
    puts " "
    puts "     +----------------------------------------------+"
    print "     + Score Player 2: " "#{p2[0]} " "---------------------------+"
    puts " "
    puts "     +----------------------------------------------+"
    show_mainboard(f1, f2, f3, f4 )
    puts " "
    puts "     -------------------- Info ----------------------"
    puts " "
end

def board_gen(r1,r2,r3,r4)    
    bag = Array.new(10) { rand(10...99) }     
    
    r1.insert(0, bag[3])
    r1.insert(1, bag[8])
    r1.insert(2, bag[4])
    r1.insert(3, bag[9])
    r1.insert(4, bag[5])

    r4.insert(0, bag[6])
    r4.insert(1, bag[7])
    r4.insert(2, bag[0])
    r4.insert(3, bag[1])
    r4.insert(4, bag[2])
    
    r2.insert(0, bag[2])
    r2.insert(1, bag[1])
    r2.insert(2, bag[0])
    r2.insert(3, bag[7])
    r2.insert(4, bag[6])
    
    r3.insert(0, bag[5])
    r3.insert(1, bag[9])
    r3.insert(2, bag[4])
    r3.insert(3, bag[8])
    r3.insert(4, bag[3])
end

def ask_move( token )
    print "     Juega: #{token} en fila: "
    row_move = get_keypressed
    print "#{row_move} y columna: "
    col_move = get_keypressed
    puts col_move
    move = [row_move.to_i - 1, col_move.to_i - 1]    
end
  
def is_available( f1,f2,f3,f4, move )

    if ((move[0]==0)&&(move[1]==0))
        if (f1[0]=="XX")
            return true
        end
    elsif ((move[0]==0)&&(move[1]==1))
        if (f1[1]=="XX")
            return true
        end    
    elsif ((move[0]==0)&&(move[1]==2))
        if (f1[2]=="XX")
            return true        
        end
    elsif ((move[0]==0)&&(move[1]==3))
        if (f1[3]=="XX")
            return true        
        end
    elsif ((move[0]==0)&&(move[1]==4))
        if (f1[4]=="XX")
            return true        
        end
    elsif ((move[0]==1)&&(move[1]==0))
        if (f2[0]=="XX")
            return true        
        end
    elsif ((move[0]==1)&&(move[1]==1))
        if (f2[1]=="XX")
            return true        
        end
    elsif ((move[0]==1)&&(move[1]==2))
        if (f2[2]=="XX")
            return true        
        end
    elsif ((move[0]==1)&&(move[1]==3))
        if (f2[3]=="XX")
            return true        
        end
    elsif ((move[0]==1)&&(move[1]==4))
        if (f2[4]=="XX")
            return true        
        end
    elsif ((move[0]==2)&&(move[1]==0))
        if (f3[0]=="XX")
            return true        
        end
    elsif ((move[0]==2)&&(move[1]==1))
        if (f3[1]=="XX")
            return true        
        end
    elsif ((move[0]==2)&&(move[1]==2))
        if (f3[2]=="XX")
            return true        
        end
    elsif ((move[0]==2)&&(move[1]==3))
        if (f3[3]=="XX")
            return true        
        end
    elsif ((move[0]==2)&&(move[1]==4))
        if (f3[4]=="XX")
            return true        
        end
    elsif ((move[0]==3)&&(move[1]==0))
        if (f4[0]=="XX")
            return true        
        end
    elsif ((move[0]==3)&&(move[1]==1))
        if (f4[1]=="XX")
            return true        
        end
    elsif ((move[0]==3)&&(move[1]==2))
        if (f4[2]=="XX")
            return true        
        end
    elsif ((move[0]==3)&&(move[1]==3))
        if (f4[3]=="XX")
            return true        
        end
    elsif ((move[0]==3)&&(move[1]==4))
        if (f4[4]=="XX")
            return true        
        end    
    else
        puts "Posicion no disponible"
      puts "ENTER para reintentar..."
      get_keypressed
      return false
    end
end
  
  # Realiza la jugada de un token dado
  def play_token( f1,f2,f3,f4,r1,r2,r3,r4, token, p1, p2)
    move = ""
    move2= ""       

    loop do
      update_screen( f1,f2,f3,f4,p1,p2)      
      move = ask_move( token )      
      break if is_available( f1,f2,f3,f4, move )
    end

    show_card(f1,f2,f3,f4,r1,r2,r3,r4, move)

    loop do
        update_screen( f1,f2,f3,f4,p1,p2 )
        move2 = ask_move( token )      
        break if is_available( f1,f2,f3,f4, move2 )
    end
    
    show_card(f1,f2,f3,f4,r1,r2,r3,r4, move2)

    if (check_match(f1,f2,f3,f4,r1,r2,r3,r4, move, move2))       
        if(token=="Jugador 1")
            resultado = p1[0].to_i + 1            
            p1[0] = resultado.to_s                        
        else
            resultado = p2[0].to_i + 1            
            p2[0] = resultado.to_s                                    
        end

        if ((p1[0].to_i)+(p2[0].to_i)==10)
            if((p1[0].to_i)>(p2[0].to_i))
                token_wins("Jugador 1")
                return true
            elseif ((p1[0].to_i)==(p2[0].to_i))
                token_wins("Empate . . .")
                return true
            else
                token_wins("Jugador 2")
                return true
            end
        end
    else
        cover_card(f1,f2,f3,f4,r1,r2,r3,r4, move)
        cover_card(f1,f2,f3,f4,r1,r2,r3,r4, move2)
    end
    
    return false
  end

# Inicia un nuevo juego completo
def match     

    r1 = []
    r2 = []
    r3 = []
    r4 = []

    p1 = ["0"]
    p2 = ["0"]

    f1 = ["XX", "XX", "XX", "XX", "XX"]
    f2 = ["XX", "XX", "XX", "XX", "XX"]
    f3 = ["XX", "XX", "XX", "XX", "XX"]
    f4 = ["XX", "XX", "XX", "XX", "XX"]

    board_gen(r1,r2,r3,r4)    

    tokens = ["Jugador 1", "Jugador 2"]
    current_turn = 0    

    loop do
      current_turn = next_turn( current_turn )
  
      #check winners
      winner = play_token( f1,f2,f3,f4,r1,r2,r3,r4, tokens[current_turn], p1, p2)
  
      break if winner
    end
  end
  
  # Ask the user if continue or quit game
  def quit_game
    puts "ENTER -> Nuevo juego"
    puts "  \"S\" -> Quitar juego" 
  
    op = get_keypressed
    return op.downcase == "s"
  end

# Inicio de juego
def game   
    
      loop do
        match
    
        break if ( quit_game )
      end
      puts
    end

def menu    

	loop do
		system("clear")
        puts "    ** Bienvenido a Mememory Match **"
        puts " "
		puts "1.- Iniciar Juego"
		puts "2.- Leader Board"		
		puts "S.- Salir"
		print "Su opcion: "
		op = gets.chomp

		case op
            when "1" then                
				game				
				gets
			when "2" then
				print "Sin implementar"				
				gets			
			when "s" then
				puts "Hasta luego..."
			else
				puts "Opcion invalida"
		end

		break if op.downcase == "s"
	end
end

menu

